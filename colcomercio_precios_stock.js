const clientId = Deno.env.get('CLIENT_ID')
const clientIntegrationId = Deno.env.get('CLIENT_INTEGRATION_ID')
const clientEcommerce = JSON.parse(Deno.env.get('CLIENT_ECOMMERCE'))


const ftpUsername = Deno.env.get('FTP_USERNAME')
const ftpPassword = Deno.env.get('FTP_PASSWORD')
const gpgKey = Deno.env.get('GPG_KEY')

async function listFiles(folder, pattern) {
  const process = Deno.run({
    cmd: ['bash'],
    stdout: 'piped',
    stdin: 'piped'
  });
  try {
    const encoder = new TextEncoder();
    const decoder = new TextDecoder();

    const command = `curl  --user '${ftpUsername}:${ftpPassword}' -l -k 'sftp://200.14.238.86:22${folder}' | grep '${pattern}'`;
    await process.stdin.write(encoder.encode(command));

    await process.stdin.close();
    const output = await process.output()
    return decoder.decode(output).split(`\n`).filter(x => {
      return x != null && x != '';
    });
  } catch (e) {
    console.log(`Could not fetch files, error: ${e.message}`)
    throw e
  } finally {
    process.close()
  }
}

async function getFile(folder, file) {
  const process = Deno.run({
    cmd: ['bash'],
    stdout: 'piped',
    stdin: 'piped'
  });
  try {
    const encoder = new TextEncoder();
    const decoder = new TextDecoder();
    const command = `curl  -k 'sftp://200.14.238.86:22${folder}${file}' --user '${ftpUsername}:${ftpPassword}' --output - | gpg --pinentry-mode=loopback --passphrase  "${gpgKey}" -d`;
    await process.stdin.write(encoder.encode(command));
    await process.stdin.close();
    const output = await process.output()
    return decoder.decode(output)
  } catch (e) {
    console.log(`Could not fetch file: ${file}, error: ${e.message}`)
    throw e
  } finally {
    process.close()
  }
}

async function moveFile(folder, file) {
  const process = Deno.run({
    cmd: ['bash'],
    stdout: 'piped',
    stdin: 'piped'
  });
  try {
    const encoder = new TextEncoder();
    const command = `curl -k 'sftp://200.14.238.86:22${folder}' --user '${ftpUsername}:${ftpPassword}' -Q "-RENAME ${folder}${file} ${folder}procesados/${file}-${new Date().toISOString()}"`;
    await process.stdin.write(encoder.encode(command));
    await process.stdin.close();
    await process.output()
  } catch (e) {
    console.log(`Could not move file: ${file}, error: ${e.message}`)
    throw e
  } finally {
    process.close()
  }
}

const init = async () => {
  const stockGroups = /^DISP(?<yyyy>\d{4})(?<MM>\d{2})(?<dd>\d{2})_(?<hh>\d{2}):(?<mm>\d{2}):(?<ss>\d{2}).*\.gpg$/
  const priceGroups = /^Lista_(?<dd>\d{2})(?<MM>\d{2})(?<yyyy>\d{4})_(?<hh>\d{2})(?<mm>\d{2}).*\.gpg$/
  let stockFiles = await listFiles('/home/sagal/', '^DISP.*\.gpg$')
  let priceFiles = await listFiles('/home/sagal/', '^Lista.*\.gpg$')
  stockFiles.sort((a, b) => {
    const aGroups = a.match(stockGroups)
    const bGroups = b.match(stockGroups)
    const aDate = new Date(aGroups.groups.yyyy, aGroups.groups.MM, aGroups.groups.dd, aGroups.groups.hh, aGroups.groups.mm, aGroups.groups.ss, 0)
    const bDate = new Date(bGroups.groups.yyyy, bGroups.groups.MM, bGroups.groups.dd, bGroups.groups.hh, bGroups.groups.mm, bGroups.groups.ss, 0)
    return aDate.getTime() - bDate.getTime()
  })
  priceFiles.sort((a, b) => {
    const aGroups = a.match(priceGroups)
    const bGroups = b.match(priceGroups)
    const aDate = new Date(aGroups.groups.yyyy, aGroups.groups.MM, aGroups.groups.dd, aGroups.groups.hh, aGroups.groups.mm, 0, 0)
    const bDate = new Date(bGroups.groups.yyyy, bGroups.groups.MM, bGroups.groups.dd, bGroups.groups.hh, bGroups.groups.mm, 0, 0)
    return aDate.getTime() - bDate.getTime()
  })
  let articleMap = {}

  await processStockFiles(stockFiles, articleMap)
  await processPriceFiles(priceFiles, articleMap)

  for (let file of stockFiles) {
    await moveFile('/home/sagal/', file)
  }

  for (let file of priceFiles) {
    await moveFile('/home/sagal/', file)
  }

  for (let article of Object.values(articleMap)) {
    await sagalDispatch(article)
  }
}

function createPayload(sku) {
  return {
    sku: sku,
    client_id: clientId,
    integration_id: clientIntegrationId,
    options: {
      merge: false
    },
    ecommerce: Object.values(clientEcommerce).map(ecommerce_id => {
      return {
        ecommerce_id: ecommerce_id,
        variants: [],
        properties: []
      }
    })
  }
}


async function processStockFiles(stockFiles, articleMap) {
  const actualStock = stockFiles[stockFiles.length - 1]
  console.log(`Read stock file: ${actualStock}`)
  const data = await getFile('/home/sagal/', actualStock)
  const articles = data.split("\n")
  for (let line of articles) {
    try {
      if (line) {
        const articleData = line.split(";")
        let sku = articleData[0]
        let stock = parseFloat(articleData[1]) <= 10 ? 0.0 : parseFloat(articleData[1])
        if (sku && sku != '') {
          if (!articleMap[sku]) {
            articleMap[sku] = createPayload(sku)
          }
          for (let element of articleMap[sku].ecommerce) {
            element.properties.push({ "stock": stock })
          }
        }
      }
    } catch (e) {
      console.log(`Could not process line: ${line} message: ${e.message}`)
    }
  }
}

async function processPriceFiles(priceFiles, articleMap) {
  const actualPrice = priceFiles[priceFiles.length - 1]
  console.log(`Read price file: ${actualPrice}`)
  const data = await getFile('/home/sagal/', actualPrice)
  const articles = data.split("\n")
  for (let line of articles) {
    try {
      if (line) {
        const articleData = line.split(";")
        let sku = articleData[2]
        let price_without_iva = articleData[3]
        let price_with_iva = articleData[5]
        if (sku && sku != '') {
          if (!articleMap[sku]) {
            articleMap[sku] = createPayload(sku)
          }
          for (let element of articleMap[sku].ecommerce) {
            element.properties.push(
              {
                "price": {
                  "value": parseFloat(price_with_iva),
                  "currency": "$",
                },
              })
            element.properties.push(
              {
                "metadata": {
                  "price_without_iva": price_without_iva,
                  "percentage_iva": `${Math.round((parseFloat(price_with_iva) / parseFloat(price_without_iva) + Number.EPSILON) * 100) / 100}`
                },
              })
          }
        }
      }
    } catch (e) {
      console.log(`Could not process line: ${line} message: ${e.message}`)
    }
  }
}

await init()

